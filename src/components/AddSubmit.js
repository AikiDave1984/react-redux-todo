import React from 'react';

let AddSubmit = (props) => {
    let input;

    
    return (
        <div>
            <form 
                onSubmit={e=> props.onSubmit(e,input)}
            >
                <input 
                    ref={node => {
                        input = node
                    }}
                />
                <button type="submit">
                    {props.buttonTitle}
                </button>
            </form>
        </div>
    )
}

export default AddSubmit;