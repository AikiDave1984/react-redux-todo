import React from 'react';
import AppSubmit from '../src/components/AddSubmit';

describe('AppSubmit component', () =>{
    it('should render an AppSubmit component', () =>{
        const wrapper = shallow(<AppSubmit />);
        expect(wrapper.html()).to.equal(`<div><form><input/><button type="submit"></button></form></div>`);
    });
});