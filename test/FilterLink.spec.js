import {mapStateToProps,mapDispatchToProps,FilterLink} from '../src/containers/FilterLink';
import {setVisibilityFilter} from '../src/actions';
import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

describe('FilterLink item', () => {
    it('should map the state to props', () =>{
        let state = {visibilityFilter:'SHOW_ALL'};
        let ownProps = {filter:'SHOW_ALL'}
        expect(mapStateToProps(state,ownProps)).to.deep.equal({active:true});
    });

    it('should map dispatch to props', () =>{

        let dispatch =spy();
        let ownProps = {filter:'SHOW_ALL'};
        mapDispatchToProps(dispatch,ownProps).onClick();

        let visibilityFilterAction = setVisibilityFilter('SHOW_ALL');

        expect(dispatch.called).to.be.true;
        expect(dispatch.getCall(0).args[0]).to.deep.equal(visibilityFilterAction);
    });

    it('renders as a <FilterLink filter="SHOW_ACTIVE">... container component - holding active span link', () => {
        const mockState = {
            visibilityFilter:'SHOW_ACTIVE'
        };
        const mockStore = configureStore();
        const store = mockStore(mockState);

        const wrapper = mount(
            <Provider store={store}>
                <FilterLink filter="SHOW_ACTIVE">
                    Active
                </FilterLink >
            </Provider>
        );

        expect(wrapper.find('span').type()).to.equal('span');
        expect(wrapper.find('span').length).to.equal(1);
        
    });

    it('renders a <FilterLink filter="SHOW_COMPLETE"> container component - holding inactive complete link', () => {
        const mockState = {
            visibilityFilter:'SHOW_ACTIVE'
        };
        const mockStore = configureStore();
        const store = mockStore(mockState);

        const wrapper = mount(
            <Provider store={store}>
                <FilterLink filter="SHOW_COMPLETE">
                    Completed
                </FilterLink >
            </Provider>
        );

        expect(wrapper.find('a').type()).to.equal('a');
        expect(wrapper.find('a').length).to.equal(1);
        
    });
}); 