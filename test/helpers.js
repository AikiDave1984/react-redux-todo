
import {sinon,spy} from 'sinon';

import { expect } from 'chai';

import { configure,mount, render, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';

configure({adapter:new Adapter()});



global.expect = expect;
global.sinon = sinon;
global.spy = spy;

global.mount = mount;
global.render = render;
global.shallow = shallow;
global.toJson = toJson;