import React from 'react';
import todos  from '../src/reducers/todos';
import visibilityFilter from '../src/reducers/visibilityFilter';
import {addTodo,setVisibilityFilter,toggleTodo}  from '../src/actions';

describe('todos reducer item', function(){

    

    it('should return the default state object if no action type is defined', ()=>{
        let mockState = [];
        let todoAction = {};
        expect(todos(mockState,todoAction)).to.deep.equal([]);
    });

    it('should create a new todo and add to the state object', ()=>{
        let mockState = [];
        let todoAction = addTodo('adding task');
        expect(todos(mockState,todoAction)).to.deep.equal([{id:2,text:'adding task',completed:false}]);
    });

    it('should toggle the complete status of a todo and edit the state object', ()=>{
        let mockState = [{id:2,text:'adding task',completed:false}];
        let todoAction = toggleTodo(2);
        expect(todos(mockState,todoAction)).to.deep.equal([{id:2,text:'adding task',completed:true}]);
    });
})

describe('visibilityfilter reducer item', () =>{
    it('should toggle the visibilty state ', () => {
        let mockState = 'SHOW_ALL';
        let visibilityAction = setVisibilityFilter("SHOW_ACTIVE");
        expect(visibilityFilter(mockState,visibilityAction)).equal("SHOW_ACTIVE");
    });
});