import React from 'react';
import Todo from '../src/components/Todo';



describe('Todo item', () =>{

    it('should be a li item',()=>{

        const wrapper = shallow(<Todo  />);
        expect(wrapper.type()).to.equal('li');
    });

    it('should have a function onclick prop',() =>{
       
        var onClickFunc = spy();
        const wrapper = shallow(<Todo onTodoClick={onClickFunc} completed={true} text={"Hello..."} />);
        wrapper.simulate('click'); 
        expect(onClickFunc.called).to.be.true;
    });

    it('should have a string text prop',() =>{

        const wrapper = shallow(<Todo  text={"Hello..."} />);
        expect(wrapper.text()).to.equal("Hello...");
    });



    it('should decorate with line through when complete prop is true ',() =>{

        const wrapper = shallow(<Todo completed={true} text={"Hello..."} />);
        expect(wrapper.get(0).props.style.textDecoration).to.equal('line-through');

    });

    it('should decorate with none when complete prop is false ',() =>{

        const wrapper = shallow(<Todo completed={false} text={"Hello..."} />);
        expect(wrapper.get(0).props.style.textDecoration).to.equal('none');
    });
})