import React from 'react';
import Link from '../src/components/Link';

describe('Link item', () => {
    it('should be an a element if link inactive',()=> {
        const onClickFunc = spy();
        const wrapper = shallow(<Link active={false} children={"childrenString"} onClick={onClickFunc}/>);
        expect(wrapper.type()).to.equal('a');
    });

    it('should be a span element if link activee', () => {
        const onClickFunc = spy();
        const wrapper = shallow(<Link active={true} children={"childrenString"} onClick={onClickFunc} />);
        expect(wrapper.type()).to.equal('span');
    });

    it('should set the containing string as the anchor text', () =>{

        const onClickFunc = spy();
        const wrapper = shallow(<Link active={true} children={"childrenString"} onClick={onClickFunc}/>);
        expect(wrapper.text()).to.equal("childrenString");

    })

    it('have an onclick handler for inactive link', () =>{
        
        var onClickFunc = spy();
        const wrapper = shallow(<Link active={false} children={"childrenString"} onClick={onClickFunc}/>);
        wrapper.simulate('click', { preventDefault() {} }); 
        expect(onClickFunc.called).to.be.true;

    })
})