import React from 'react';
import {addTodo,setVisibilityFilter,toggleTodo}  from '../src/actions';

describe('actions addTodo object',() => {
    it('should create an addTodo action', ()=>{
        expect(addTodo('todo text')).to.deep.equal({id:0,text:'todo text',type:'ADD_TODO'});
        expect(addTodo('todo text1')).to.deep.equal({id:1,text:'todo text1',type:'ADD_TODO'});
    })
});

describe('actions setVisibilityFilter object',() => {
    it('should create a set visibility action', ()=>{
        expect(setVisibilityFilter('SHOW_ALL')).to.deep.equal({filter:'SHOW_ALL',type:'SET_VISIBLITY_FILTER'});
    })
});

describe('actions toggleTodo object',() => {
    it('should create a toggleTodo action', ()=>{
        expect(toggleTodo(2)).to.deep.equal({id:2,type:'TOGGLE_TODO'});
    })
});