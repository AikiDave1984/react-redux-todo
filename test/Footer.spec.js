import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import Footer from '../src/components/Footer';

describe('Footer Component', () =>{
    it('should render as a p',()=>{

        const mockState = {
            visibilityFilter:'SHOW_ACTIVE'
        };
        const mockStore = configureStore();
        const store = mockStore(mockState);

        const wrapper = mount(
            <Provider store={store}>
                <Footer />
            </Provider>
        );

        expect(wrapper.html()).to.equal('<p>Show : <a href="#">All</a>, <span>Active</span>, <a href="#">Completed</a></p>');
    });

});