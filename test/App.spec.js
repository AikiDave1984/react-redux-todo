import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import App from '../src/components/app';

describe('App component', () =>{
    it('should render an App component', () =>{
        let mockState = {todos:[
            {id:1,text:'Must do dishes...','completed':false},
            {id:2,text:'Must do dishes...','completed':true}
        ],filter:'SHOW_ALL'};
        const mockStore = configureStore();
        const store = mockStore(mockState);

        const wrapper = mount(
            <Provider store={store}>
                <App />
            </Provider>
        );

        //Uncomment below to show html string and copy to variable to test rendering
        //console.log(wrapper.html()) 
        let expectedHTML = '<div><div><form><input><button type="submit">Add Todo</button></form></div><ul><li style="text-decoration: none;">Must do dishes...</li><li style="text-decoration: line-through;">Must do dishes...</li></ul><p>Show : <a href="#">All</a>, <a href="#">Active</a>, <a href="#">Completed</a></p></div>';


        expect(wrapper.html()).to.equal(expectedHTML);
    })
})