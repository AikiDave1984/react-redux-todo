import {getVisibleTodos,mapStateToProps,mapDispatchToProps,VisibleTodoList} from '../src/containers/VisibleTodoList';
import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import {toggleTodo} from '../src/actions';

describe('getVisisbleTodos function', () =>{
    it('should return entire todo list when filtered by SHOW_ALL',() =>{
        let filter = 'SHOW_ALL';
        let todos = [{id:1,text:'Must do dishes...','completed':false}];
        expect(getVisibleTodos(todos,filter)).to.deep.equal(todos);
    });

    it('should return entire todo list when filtered by SHOW_COMPLETED',() =>{
        let filter = 'SHOW_COMPLETED';
        let todos = [
            {id:1,text:'Must do dishes...','completed':false},
            {id:2,text:'Must do dishes...','completed':true}
        ];

        let todosResult = [
            {id:2,text:'Must do dishes...','completed':true},
        ];
        expect(getVisibleTodos(todos,filter)).to.deep.equal(todosResult);
    });

    it('should return entire todo list when filtered by SHOW_ACTIVE',() =>{
        let filter = 'SHOW_ACTIVE';
        let todos = [
            {id:1,text:'Must do dishes...','completed':false},
            {id:2,text:'Must do dishes...','completed':true}
        ];

        let todosResult = [
            {id:1,text:'Must do dishes...','completed':false},
        ];
        expect(getVisibleTodos(todos,filter)).to.deep.equal(todosResult);
    });
});

describe('mapStateToProps function',() =>{
    it('returns an object with todos', () => {
        let state = {todos:[
            {id:1,text:'Must do dishes...','completed':false},
            {id:2,text:'Must do dishes...','completed':true}
        ],filter:'SHOW_COMPLETED'};

        let expectedStateObj = {
            todos:[
                {
                    id:2,
                    text:'Must do dishes...',
                    completed:true
                }
            ]
        }

        expect(mapStateToProps(state)).to.deep.equal(expectedStateObj);
    });
});

describe('mapDispatchToProps function',() =>{
    it('returns an object with onTodoClick property', () => {
        let dispatch = (callback) => {callback(id)};
        expect(mapDispatchToProps(dispatch)).to.have.property('onTodoClick');
    });
});

describe('VisibleTodoList function', ()=>{



    it('should render as a <VisibleTodoList /> container component',()=>{

        let mockState = {todos:[
            {id:1,text:'Must do dishes...','completed':false},
            {id:2,text:'Must do dishes...','completed':true}
        ],filter:'SHOW_ALL'};

        const mockStore = configureStore();
        const store = mockStore(mockState);

        const wrapper = mount(
            <Provider store={store}>
                <VisibleTodoList />
            </Provider>
        );

        expect(wrapper.find('ul').length).to.equal(1);
        expect(wrapper.find('li').length).to.equal(2);

    })
})