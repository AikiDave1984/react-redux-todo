import React from 'react';
import TodoList from '../src/components/TodoList';
import Todo from '../src/components/Todo';
describe('TodoList item', () => {
    it('should be a ul element', ()=>{

        const todos = [{
            id: 0,
            text: 'hello',
            completed: false
          }];

          var onClickFunc = spy();

        const wrapper= shallow(<TodoList todos={todos} onTodoClick={onClickFunc } />);
        expect(wrapper.type()).to.equal('ul');
    })

    it('should render Todo child element',() => {
        const todos = [{
            id: 0,
            text: 'hello',
            completed: false
          }];

        var onClickFunc = spy();
        const wrapper= shallow(<TodoList todos={todos} onTodoClick={onClickFunc }  />);
       
        expect(wrapper.find(Todo)).to.have.length(1);
    })
});